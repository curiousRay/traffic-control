library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity controller is
  port (clk, rst: in std_logic;
    r1, r2, y1, y2, g1, g2: out std_logic; --lights signal
    w1, w2, w3: in std_logic; -- counters working signal
    c1, c2, c3: out std_logic -- enable counters
  );
end controller;

architecture beh of controller is
  type state_space is (s0, s1, s2, s3);
  signal state: state_space;
begin
process(clk)
  begin
  if rst = '1' then
    state <= s0;
  elsif (clk'event and clk = '1') then -- according to the flowchart
    case state is
      when s0 => if w1 = '1' then
        state <= s1;
      end if;
      when s1 => if w2 = '1' then
        state <= s2;
      end if;
      when s2 => if w3 = '1' then
        state <= s3;
      end if;
      when s3 => if w2 = '1' then
        state <= s0;
      end if;
    end case;
  end if;
  end process;
  
  c1 <= '1' when state = s0 else '0'; -- define states
  c2 <= '1' when state = s1 or state = s3 else '0';
  c3 <= '1' when state = s2 else '0';
  
  r1 <= '1' when state = s1 or state = s0 else '0';
  y1 <= '1' when state = s3 else '0';
  g1 <= '1' when state = s2 else '0';

  r2 <= '1' when state = s2 or state = s3 else '0';
  y2 <= '1' when state = s1 else '0';
  g2 <= '1' when state = s0 else '0';
 
 end beh;
