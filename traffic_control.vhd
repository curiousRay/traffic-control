library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity traffic_control is
  port (clk, rst: in std_logic;
    r1, r2, y1, y2, g1, g2: out std_logic --lights signal
  );
end traffic_control;

architecture beh of traffic_control is
  
  signal c1, c2, c3: std_logic;
  signal w1, w2, w3: std_logic;
  
  component count26 is
    port (clk, enable: in std_logic;
      c: out std_logic
    );
  end component;
  component count05 is
    port (clk, enable: in std_logic;
      c: out std_logic
    );
  end component;
  component count30 is
    port (clk, enable: in std_logic;
      c: out std_logic
    );
  end component;
  component controller is
    port (clk, rst: in std_logic;
    r1, r2, y1, y2, g1, g2: out std_logic; --lights signal
    w1, w2, w3: in std_logic; -- counters working signal
    c1, c2, c3: out std_logic -- enable counters
  );
  end component;
  
  begin
  ctrl: controller port map(clk, rst, r1, r2, y1, y2, g1, g2, w1, w2, w3, c1, c2, c3);
  cout1: count30 port map(clk, c1, w1);
  cout2: count05 port map(clk, c2, w2); 
  cout3: count26 port map(clk, c3, w3);
  
end beh;
