library ieee;
use ieee.std_logic_1164.all;

entity count26 is
  port (clk, enable: in std_logic;
    c: out std_logic
  );
end count26;

architecture beh of count26 is
  begin
  process(clk)
    variable cnt: Integer range 26 downto 0;
    begin
    if (clk' event and clk = '1') then
      if enable = '1' and cnt < 26 then
        cnt := cnt + 1;
      else cnt := 0;
      end if;
    end if;
    if cnt = 26 then
      c <= '1';
    else c <= '0';
    end if;
  end process;
end beh;