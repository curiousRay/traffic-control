library ieee;
use ieee.std_logic_1164.all;

entity count05 is
  port (clk, enable: in std_logic;
    c: out std_logic
  );
end count05;

architecture beh of count05 is
  begin
  process(clk)
    variable cnt: Integer range 5 downto 0;
    begin
    if (clk' event and clk = '1') then
      if enable = '1' and cnt < 5 then
        cnt := cnt + 1;
      else cnt := 0;
      end if;
    end if;
    if cnt = 5 then
      c <= '1';
    else c <= '0';
    end if;
  end process;
end beh;